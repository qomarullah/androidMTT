package com.mtt.universal.providers.today;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mtt.universal.MainActivity;
import com.mtt.universal.R;
import com.mtt.universal.inherit.BackPressFragment;
import com.mtt.universal.inherit.CollapseControllingFragment;
import com.mtt.universal.util.Helper;

/**
 * This fragment is used to play live video streams.
 */
public class TodayFragmentOld extends Fragment implements CollapseControllingFragment, BackPressFragment {

    private Activity mAct;
    private RelativeLayout rl;
    SwipeRefreshLayout mSwipeRefreshLayout;
    TextView text1;
    /** Called when the activity is first created. */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rl = (RelativeLayout) inflater.inflate(R.layout.fragment_today, container, false);
        text1 = (TextView) rl.findViewById(R.id.text1);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rl.findViewById(R.id.refreshlayout);


        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //browser.reload();
                refreshList();
            }
        });

        return rl;
    }


    public void refreshList(){
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }, 5000);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mAct = getActivity();

        Helper.isOnlineShowDialog(mAct);

        String param1 = this.getArguments().getStringArray(MainActivity.FRAGMENT_DATA)[0];
        text1.setText(param1);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
    }



    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean supportsCollapse() {
        return false;
    }

    @Override
    public boolean handleBackPress() {
        return false;
    }

}


